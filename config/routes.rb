Rails.application.routes.draw do
  root to: 'welcome#index'
  get '/index', to: 'welcome#index'
	
  resources :users, only: [:new, :create]
  get '/registrate', to: 'users#new'
  get '/ranking', to: 'users#show'

  resources :guides
  get '/guides', to: 'guides#index'
  #post '/users/:id/guides/new', to: 'guides#new'

  resources :profiles

  resource :session, only: [:new, :create, :destroy]
  get 'login' => "sessions#new", as: "login"
  get 'logout' => "sessions#destroy", as: "logout"
  #resources :profiles do
  #  member do
  #    get :edit
  #  end
  #end
end
