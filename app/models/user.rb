require 'digest'

class User < ActiveRecord::Base
  attr_accessor :password
  before_save :encrypt_new_password
  after_create :build_profile

  has_one :profile
  has_many :guides, ->{order('updated_at DESC')}, dependent: :nullify
  has_many :replies, through: :guides, source: :comments

  scope :rank, -> {select('users.id, users.name, profiles.points').joins(:profile).order('points DESC').limit(10)}

  def build_profile
    Profile.create(user: self, points: 0, bio: "Hey im here using guiajuegos", categorie:"Novato", photo: '/assets/images/usuario.jpg')
  end
	
  validates :email, uniqueness: {case_sensitive: false, message: 'El correo introducido no es único'}, length: {in: 12..20, too_short: "debe tener al menos 12 caracteres"}, format: {multiline: true,with: /^.+@.+$/, message: "formato de correo no valido"}
  validates :name, uniqueness: {case_sensitive: true, message:'Este nombre ya esta siendo usado'}, length: {in: 4..10, too_short: "El nombre debe tener al menos 4 caracteres"}
  validates :password, confirmation: true, length: {within: 6..20}, presence: true
  validates :password_confirmation, presence: true

  def self.authenticate(name,password)
    user = find_by_name(name)
    return user if user && user.authenticated?(password)
  end
  
  def authenticated?(password)
    self.hashed_password == encrypt(password)
  end
  
  protected
  def encrypt_new_password
    return if password.blank?
    self.hashed_password = encrypt(password)
  end
  def password_required?
    hashed_password.blank? || password.present?
  end
  def encrypt(string)
    Digest::SHA1.hexdigest(string) 
  end

end
