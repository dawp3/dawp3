class Comment < ApplicationRecord
	belongs_to :guide
 
	scope :published, ->{where("comments.published_at IS NOT NULL")}

	validates :published_at, presence: true
end
