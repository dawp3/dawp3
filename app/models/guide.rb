class Guide < ApplicationRecord
  mount_uploader :photo, ImageUploader

  belongs_to :user, optional: true
  has_many :comments

  scope :guide, -> {select('guides.id, guides.user_id, guides.photo, guides.title, guides.body, guides.updated_at, users.name').joins(:user)}
  scope :guide_list, -> {select('guides.id, guides.user_id, guides.photo, guides.title, guides.updated_at, guides.trophies, guides.category, users.name').joins(:user).limit(10)}
  validates :body, :title, :trophies, :category, presence: {message: "el campo no puede quedar vacío"}
  validates :photo, presence: {message: "Debe añadir una foto"}
 
end
