class CommentsController < ApplicationController

  before_action :load_guide, except: :destroy
  before_action :authenticate, only: :destroy
  
  def create
    @comment = @guide.comments.new(comment_params)
    if @comment.save
      redirect_to @guide, notice: 'gracias por su comentario'
    else
      redirect_to @guide, alert: 'no ha sido posible añadir el comentario'
    end
  end
  
  def destroy
    @guide = current_user.guides.find_by_id(params[:guide_id])
    if @guide.nil?
       redirect_to :index, notice: "operación no permitida"
     end
    @comment = @guide.comments.find_by_id(params[:id])
    @comment.destroy
    redirect_to @guide, notice: "comentario eliminado"
  end
  
  private
    def load_guide
      @guide = Guide.find_by_id(params[:guide_id])
    end
    
    def comment_params
      params.require(:comment).permit(:name, :email, :body)
    end
end
