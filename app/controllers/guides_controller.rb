class GuidesController < ApplicationController
  before_action :set_guide, only: [:show]

  def new 
    @guide = Guide.new
  end

  def create
    @guide = current_user.guides.new(guide_params)

    respond_to do |format|
      if @guide.save
        format.html { redirect_to @guide, notice: 'Guía creada correctamente.' }
      else
        format.html { render :new }
      end
    end
  end
  

  def edit
   @guide = Guide.find_by_id(params[:id])
   redirect_to root_path, alert: "Operación no permitida." unless @guide
  end

  def update
    @guide = Guide.find_by_id(params[:id])
    respond_to do |format|
      if @guide.update(guide_params)
        format.html { redirect_to @guide, notice: 'Guía Actualizada.' }
      else
        format.html { render :edit }
      end
    end
  end 

 def destroy
    @guide = current_user.guides.find(params[:id])
    @guide.destroy
    respond_to do |format|
      format.html { redirect_to guides_url, notice: 'La guía fue eliminada.' }
    end
  end
  
  def index
   @guides = Guide
		.guide_list	

  end 

  def show
    redirect_to({action: :index}, notice: 'Guía no encontrada.') unless @guide
  end

  private
  def set_guide
      @join = Guide
		  .guide
      @guide = @join.find_by_id(params[:id])
  end

  def guide_params
      params.require(:guide).permit(:title, :body, :photo, :category,:trophies,:user_id)
  end
end
