class RenameColumnPasswordconfirminUsertoPasswordConfirmation < ActiveRecord::Migration[5.1]
  def change
  rename_column :users, :passwordconfirm, :password_confirmation
  end
end
