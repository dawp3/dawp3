class CreateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :profiles do |t|
      t.integer :points
      t.text :bio
      t.string :categorie

      t.timestamps
    end
  end
end
